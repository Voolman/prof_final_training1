import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomTextFieldPage extends StatefulWidget {
  final String label;
  final bool enableObscure;
  final TextEditingController controller;
  final Function(String) onChanged;
  const CustomTextFieldPage({super.key, required this.label, required this.controller, required this.onChanged, required this.enableObscure});

  @override
  State<CustomTextFieldPage> createState() => _CustomTextFieldPageState();
}

class _CustomTextFieldPageState extends State<CustomTextFieldPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: Theme.of(context).textTheme.titleSmall,
        ),
        SizedBox(height: 17.w),
        SizedBox(
          width: double.infinity,
          height: 48.w,
          child: TextField(
            enabled: widget.enableObscure,
            controller: widget.controller,
            decoration: InputDecoration(
                enabledBorder: Theme.of(context).inputDecorationTheme.enabledBorder,
                focusedBorder: Theme.of(context).inputDecorationTheme.focusedBorder,
                suffixIcon: (widget.enableObscure) ? null : SvgPicture.asset('assets/confirm.svg', fit: BoxFit.scaleDown),

            ),
          ),
        )
      ],
    );
  }
}
