import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:prof2024/presentation/theme/colors.dart';


var colors = LightColorsApp();
var theme = ThemeData(
  textTheme: TextTheme(
    titleLarge: GoogleFonts.raleway(
      textStyle: TextStyle(
        color: colors.text,
        fontSize: 32.sp,
        fontWeight: FontWeight.w700,
        height: 37.57.w/32.w
      )
    ),
    titleMedium: GoogleFonts.raleway(
        textStyle: TextStyle(
            color: colors.text,
            fontSize: 16.sp,
            fontWeight: FontWeight.w400,
            height: 3.w/2.w
        )
    ),
    titleSmall: GoogleFonts.raleway(
        textStyle: TextStyle(
            color: colors.text,
            fontSize: 16.sp,
            fontWeight: FontWeight.w500,
            height: 20.w/16.w
        )
    ),
    labelSmall: GoogleFonts.raleway(
        textStyle: TextStyle(
            color: colors.hint,
            fontSize: 14.sp,
            fontWeight: FontWeight.w500,
            height: 16.w/14.w
        )
    ),
    labelLarge: GoogleFonts.raleway(
        textStyle: TextStyle(
            color: colors.text,
            fontSize: 12.sp,
            fontWeight: FontWeight.w400,
            height: 16.w/12.w
        )
    ),
    labelMedium: GoogleFonts.raleway(
        textStyle: TextStyle(
            color: colors.text,
            fontSize: 16.sp,
            fontWeight: FontWeight.w500,
            height: 18.78.w/16.w
        )
    ),
    bodyLarge: GoogleFonts.raleway(
        textStyle: TextStyle(
            color: colors.text,
            fontSize: 26.sp,
            fontWeight: FontWeight.w700,
            height: 30.52.w/26.w
        )
    ),
    bodyMedium: GoogleFonts.raleway(
        textStyle: TextStyle(
            color: colors.text,
            fontSize: 24.sp,
            fontWeight: FontWeight.w600,
            height: 28.18.w/24.w
        )
    ),
    bodySmall: GoogleFonts.raleway(
        textStyle: TextStyle(
            color: colors.text,
            fontSize: 14.sp,
            fontWeight: FontWeight.w400,
            height: 24.w/14.w
        )
    ),
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: colors.accent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16)
      ),
      textStyle: GoogleFonts.raleway(
          textStyle: TextStyle(
              color: colors.text,
              fontSize: 14.sp,
              fontWeight: FontWeight.w600,
              height: 22.w/14.w
          )
      )
    )
  ),
  inputDecorationTheme: InputDecorationTheme(
    filled: true,
    fillColor: colors.background,
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(14),
        borderSide: BorderSide(color: Colors.transparent)
    ),
    focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(14),
      borderSide: BorderSide(color: Colors.transparent)
    ),
  ),

);