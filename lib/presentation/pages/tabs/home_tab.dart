import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:prof2024/data/models/ModelProduct.dart';
import 'package:prof2024/data/storage/products.dart';
import 'package:prof2024/domain/home_tab_presenter.dart';
import 'package:prof2024/presentation/pages/cart_page.dart';
import 'package:prof2024/presentation/pages/popular.dart';
import 'package:prof2024/presentation/theme/colors.dart';
import 'package:prof2024/presentation/widgets/custom_card.dart';
import 'package:prof2024/presentation/widgets/dialogs.dart';

import '../side_menu.dart';

class HomeTab extends StatefulWidget {
  const HomeTab({super.key});

  @override
  State<HomeTab> createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  bool val = false;
  @override
  void initState(){
    super.initState();
    getAllProductsFunc(
        (res){
          setState(() {
            val = res;
          });
        },
      (String e){showError(context, e);}
    );
    getCategoryList(
        (res){
          setState(() {
            createRowCategory(res);
          });
        },
        (String e){showError(context, e);}
    );
  }

  void createRowCategory(List<Map<String, dynamic>> category){
    var colors = LightColorsApp();
    List<Widget> categoriesWidgets = [];
    for(int i = 0; i<category.length; i++){
      categoriesWidgets.add(
        Row(
          children: [
            Container(
              height: 40.w,
              width: 108.w,
              decoration: BoxDecoration(
                color: colors.block,
                borderRadius: BorderRadius.circular(14),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 2.w),
                    blurRadius: 24,
                    color: Color.fromARGB(10, 0, 0, 0),
                    spreadRadius: 0
                  )
                ]
              ),
              child: Center(
                child: Text(
                  category[i]['title'],
                  style: Theme.of(context).textTheme.labelLarge?.copyWith(height: 14.09.w/12.w, letterSpacing: 1),
                ),
              ),
            ),
            SizedBox(width: 16.w)
          ],
        )
      );
    }
    categories = categoriesWidgets;
  }
  List<Widget> categories = [];
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    List<ModelProduct> products = getPopularProducts();
    return (val) ? Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Padding(
                    padding: EdgeInsets.only(top: 43.w, left: 102.w),
                  child: GestureDetector(
                      child: SvgPicture.asset('assets/light.svg', fit: BoxFit.scaleDown,)),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 48.w, left: 20.w, right: 20.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: (){
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) => SideMenu()));
                            },
                              child: SvgPicture.asset('assets/hamburger.svg', width: 25.71.w, height: 18.w,)),
                          Text(
                            'Главная',
                            style: Theme.of(context).textTheme.titleLarge,
                          ),
                          GestureDetector(
                            onTap: (){
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartPage()));
                            },
                            child: Container(
                              height: 44,
                              width: 44,
                              decoration: BoxDecoration(
                                color: colors.block,
                                borderRadius: BorderRadius.circular(360)
                              ),
                              child: SvgPicture.asset('assets/bag.svg', color: colors.text, width: 24, height: 24, fit: BoxFit.scaleDown,),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 19.w),
                      Row(
                        children: [
                          Container(
                            height: 52.w,
                            width: 269.w,
                            decoration: BoxDecoration(
                              color: colors.block,
                              borderRadius: BorderRadius.circular(14)
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(width: 26.w,),
                                SvgPicture.asset('assets/search.svg', height: 24.w, width: 24.w, fit: BoxFit.scaleDown,),
                                SizedBox(width: 12.w),
                                SizedBox(
                                  height: 50.w,
                                  width: 100.w,
                                  child: TextField(
                                    decoration: InputDecoration(
                                      fillColor: colors.block,
                                      filled: true,
                                      hintText: 'Поиск',
                                      hintStyle: Theme.of(context).textTheme.labelSmall,
                                    ),
                                  ),
                                ),

                              ],
                            ),
                          ),
                          SizedBox(width: 14.w),
                          Container(
                            height: 52.w,
                            width: 52.w,
                            decoration: BoxDecoration(
                              color: colors.accent,
                              borderRadius: BorderRadius.circular(360)
                            ),
                            child: SvgPicture.asset('assets/sliders.svg', fit: BoxFit.scaleDown,),
                          )
                        ],
                      ),
                      SizedBox(height: 24.w),
                      Text(
                          'Категории',
                        style: Theme.of(context).textTheme.labelMedium?.copyWith(fontWeight: FontWeight.w600),
                      ),
                      SizedBox(height: 16.w,),
                      
                    ],
                  ),
                ),
              ],
            ),
            Padding(
                padding: EdgeInsets.only(left: 20.w),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 40.w,
                            width: 108.w,
                            decoration: BoxDecoration(
                                color: colors.block,
                                borderRadius: BorderRadius.circular(14),
                                boxShadow: [
                                  BoxShadow(
                                      offset: Offset(0, 2.w),
                                      blurRadius: 24,
                                      color: Color.fromARGB(10, 0, 0, 0),
                                      spreadRadius: 0
                                  )
                                ]
                            ),
                            child: Center(
                              child: Text(
                                'Все',
                                style: Theme.of(context).textTheme.labelLarge?.copyWith(height: 14.09.w/12.w, letterSpacing: 1),
                              ),
                            ),
                          ),
                          SizedBox(width: 16.w)
                        ] + categories,
                      )
                    ],
                  ),
                )
            ),
            Padding(
                padding: EdgeInsets.only(top: 24.w, left: 20.w, right: 20.w),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Популярное',
                        style: Theme.of(context).textTheme.labelSmall,
                      ),
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => Popular()));
                        },
                        child: Text(
                          'Все',
                          style: Theme.of(context).textTheme.labelLarge?.copyWith(fontWeight: FontWeight.w500, color: colors.accent),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 30.w,),
                  Row(
                    children: [
                      CustomCard(product: products[0])
                    ],
                  ),
                  SizedBox(height: 29.w,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Акции',
                        style: Theme.of(context).textTheme.labelSmall,
                      ),
                      Text(
                        'Все',
                        style: Theme.of(context).textTheme.labelLarge?.copyWith(fontWeight: FontWeight.w500, color: colors.accent),
                      )
                    ],
                  ),
                  SizedBox(height: 20.w,),
                  Container(
                    height: 95.w,
                    width: double.infinity,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    ) : Center(child: CircularProgressIndicator());
  }
}