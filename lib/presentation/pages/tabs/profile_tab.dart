import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:prof2024/presentation/theme/colors.dart';
import 'package:prof2024/presentation/widgets/custom_text_field_page.dart';
import 'package:prof2024/presentation/widgets/dialogs.dart';

import '../side_menu.dart';

class ProfileTab extends StatefulWidget {
  const ProfileTab({super.key});

  @override
  State<ProfileTab> createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {
  bool enableField = false;
  Uint8List? avatar;
  TextEditingController firstName = TextEditingController();
  TextEditingController secondName = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController phone = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.block,
      body: SingleChildScrollView(
        child: Padding(
            padding: EdgeInsets.only(top: 60.w, left: 18.w, right: 18.w),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                      onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => SideMenu()));
                      },
                      child: SvgPicture.asset('assets/hamburger.svg', width: 25.71.w, height: 18.w,)),
                  Text(
                    'Профиль',
                    style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w600),
                  ),
                  GestureDetector(
                    onTap: (){

                    },
                    child: Container(
                      height: 44,
                      width: 44,
                      decoration: BoxDecoration(
                          color: colors.block,
                          borderRadius: BorderRadius.circular(360)
                      ),
                      child: SvgPicture.asset('assets/edit.svg', width: 25, height: 25, fit: BoxFit.scaleDown,),
                    ),
                  )
                ],
              ),
              SizedBox(height: 45.w),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 96.w,
                    width: 96.w,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(360),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(360),
                      child: (avatar == null) ? SizedBox() : Image.memory(avatar!, fit: BoxFit.cover,),
                    ),
                  ),
                  Text(
                    'Эмануэль Кверти',
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(color: colors.text, fontSize: 20.sp, height: 23.48.w/20.w),
                  ),
                  SizedBox(height: 8.w),
                  GestureDetector(
                    onTap: (){
                      pickAvatarDialog(context, (res){
                        setState(() {
                          avatar = res;
                        });
                      });
                    },
                    child: Text(
                      'Изменить фото профиля',
                      style: Theme.of(context).textTheme.labelLarge?.copyWith(fontWeight: FontWeight.w600, color: colors.accent),
                    ),
                  )
                ],
              ),
              SizedBox(height: 11.w),
              SizedBox(height: 66.w),
              SizedBox(height: 20.w),
              CustomTextFieldPage(
                  label: 'Имя',
                  controller: firstName,
                  onChanged: (_){},
                  enableObscure: enableField
              ),
              SizedBox(height: 16.w),
              CustomTextFieldPage(
                  label: 'Фамилия',
                  controller: secondName,
                  onChanged: (_){},
                  enableObscure: enableField
              ),
              SizedBox(height: 16.w),
              CustomTextFieldPage(
                  label: 'Адрес',
                  controller: address,
                  onChanged: (_){},
                  enableObscure: enableField
              ),
              SizedBox(height: 16.w),
              CustomTextFieldPage(
                  label: 'Телефон',
                  controller: phone,
                  onChanged: (_){},
                  enableObscure: enableField
              ),
              SizedBox(height: 50.w),
            ],
          ),
        ),
      ),
    );
  }
}