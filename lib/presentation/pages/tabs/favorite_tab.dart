import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:prof2024/domain/favorite_presenter.dart';
import 'package:prof2024/presentation/theme/colors.dart';
import 'package:prof2024/presentation/widgets/custom_card.dart';

import '../../../data/models/ModelProduct.dart';

class FavoriteTab extends StatefulWidget {
  const FavoriteTab({super.key});

  @override
  State<FavoriteTab> createState() => _FavoriteTabState();
}

class _FavoriteTabState extends State<FavoriteTab> {
  @override
  void initState(){
    super.initState();
    products = getFavoriteProducts();
    setState(() {

    });
  }
  List<ModelProduct> products = [];
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(top: 48.w, left: 21.w, right: 15.w),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    height: 44.w,
                    width: 44.w,
                    decoration: BoxDecoration(
                        color: colors.block,
                        borderRadius: BorderRadius.circular(360)
                    ),
                    child: SvgPicture.asset('assets/back.svg', fit: BoxFit.scaleDown,),
                  ),
                ),
                Text(
                  'Sneaker Shop',
                  style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w600),
                ),
                Container(
                  height: 44.w,
                  width: 44.w,
                  decoration: BoxDecoration(
                      color: colors.block,
                      borderRadius: BorderRadius.circular(360)
                  ),
                  child: SvgPicture.asset('assets/bag.svg', fit: BoxFit.scaleDown, color: colors.text),
                ),
              ],
            ),
            Expanded(
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                    childAspectRatio: 160.w/182.w,
                    mainAxisSpacing: 20.w,
                    crossAxisSpacing: MediaQuery.sizeOf(context).width - 340.w

                  ),
                  itemCount: products.length,
                  itemBuilder: (_, index){
                    return CustomCard(product: products[index]);
                  }
              ),
            )

          ],
        ),
      ),
    );
  }
}