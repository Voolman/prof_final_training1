import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:prof2024/domain/side_menu_presenter.dart';
import 'package:prof2024/presentation/pages/cart_page.dart';
import 'package:prof2024/presentation/pages/home.dart';
import 'package:prof2024/presentation/theme/colors.dart';
import 'package:prof2024/presentation/widgets/dialogs.dart';

class SideMenu extends StatefulWidget {
  const SideMenu({super.key});

  @override
  State<SideMenu> createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.accent,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 78.w, left: 36.w),
            child: Container(
              height: 96.w,
              width: 96.w,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(360),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(360),
                child: Image.network(''),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 15.w, left: 35.w),
            child: Text(
              'Эмануэль Кверти',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(color: colors.block, fontSize: 20.sp, height: 23.48.w/20.w),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 55.w, left: 20.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Home(currentIndex: 3,)));
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset('assets/profile.svg', color: colors.block, width: 24.w, height: 24.w),
                      SizedBox(width: 22.w),
                      Text(
                        'Профиль',
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(color: colors.block),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 32.w),

                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const CartPage()));
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset('assets/bag.svg', color: colors.block, width: 24.w, height: 24.w),
                      SizedBox(width: 22.w),
                      Text(
                        'Корзина',
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(color: colors.block),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 32.w),

                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Home(currentIndex: 1)));
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset('assets/heart.svg', color: colors.block, width: 24.w, height: 24.w),
                      SizedBox(width: 22.w),
                      Text(
                        'Избранное',
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(color: colors.block),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 32.w),

                Row(
                  children: [
                    SvgPicture.asset('assets/car.svg', color: colors.block, width: 24.w, height: 24.w),
                    SizedBox(width: 22.w),
                    Text(
                      'Заказы',
                      style: Theme.of(context).textTheme.titleSmall?.copyWith(color: colors.block),
                    )
                  ],
                ),
                SizedBox(height: 32.w),

                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Home(currentIndex: 2,)));
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset('assets/notification.svg', color: colors.block, width: 24.w, height: 24.w),
                      SizedBox(width: 22.w),
                      Text(
                        'Уведомления',
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(color: colors.block),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 32.w),

                Row(
                  children: [
                    SvgPicture.asset('assets/settings.svg', color: colors.block, width: 24.w, height: 24.w),
                    SizedBox(width: 22.w),
                    Text(
                      'Настройки',
                      style: Theme.of(context).textTheme.titleSmall?.copyWith(color: colors.block),
                    )
                  ],
                ),
                SizedBox(height: 38.w),
                Divider(height: 0, color: colors.cont1),
                SizedBox(height: 30.w),
                GestureDetector(
                  onTap: (){
                    pressSignOut((){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Home()));
                    }, (p0){showError(context, p0);});
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset('assets/output.svg', color: colors.block, width: 24.w, height: 24.w),
                      SizedBox(width: 22.w),
                      Text(
                        'Выйти',
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(color: colors.block),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 32.w),
              ],
            ),
          )
        ],
      ),
    );
  }
}