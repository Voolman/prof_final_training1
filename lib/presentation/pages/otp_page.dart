import 'package:change_case/change_case.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pinput/pinput.dart';
import 'package:prof2024/domain/forgot_password_presenter.dart';
import 'package:prof2024/domain/log_in_presenter.dart';
import 'package:prof2024/domain/otp_presenter.dart';
import 'package:prof2024/domain/utils.dart';
import 'package:prof2024/presentation/pages/home.dart';
import 'package:prof2024/presentation/pages/sign_up.dart';
import 'package:prof2024/presentation/theme/colors.dart';
import 'package:prof2024/presentation/widgets/custom_text_field.dart';
import 'package:prof2024/presentation/widgets/dialogs.dart';

class OTPPage extends StatefulWidget {
  final String email;
  const OTPPage({super.key, required this.email});

  @override
  State<OTPPage> createState() => _OTPPageState();
}

class _OTPPageState extends State<OTPPage> {
  TextEditingController code = TextEditingController();
  bool enableButton = true;
  void onChanged(_){}
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.block,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 66.w, left: 20.w, right: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Home()));
                },
                child: Container(
                  height: 44.w,
                  width: 44.w,
                  decoration: BoxDecoration(
                      color: colors.background,
                      borderRadius: BorderRadius.circular(360)
                  ),
                  child: SvgPicture.asset('assets/back.svg', color: colors.text, fit: BoxFit.scaleDown,),
                ),
              ),
              SizedBox(height: 15.w,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'OTP Проверка',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  SizedBox(height: 8.w),
                  Text(
                    'Пожалуйста, Проверьте Свою\nЭлектронную Почту, Чтобы Увидеть Код\nПодтверждения',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              SizedBox(height: 16.w),
              Text(
                'OTP Код',
                style: Theme.of(context).textTheme.bodyMedium?.copyWith(fontSize: 21.sp, height: 24.65.w/21.w),
              ),
              SizedBox(height: 20.w),
              Pinput(
                length: 6,
                controller: code,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                defaultPinTheme: PinTheme(
                  height: 99.w,
                  width: 46.w,
                  textStyle: Theme.of(context).textTheme.bodyMedium?.copyWith(fontSize: 18.sp, height: 24.w/18.w),
                  decoration: BoxDecoration(
                    color: colors.background,
                    border: Border.all(color: colors.red),
                    borderRadius: BorderRadius.circular(12)
                  )
                ),
                submittedPinTheme: PinTheme(
                    height: 99.w,
                    width: 46.w,
                    textStyle: Theme.of(context).textTheme.bodyMedium?.copyWith(fontSize: 18.sp, height: 24.w/18.w),
                    decoration: BoxDecoration(
                        color: colors.background,
                        borderRadius: BorderRadius.circular(12)
                    )
                ),
                onChanged: (_){
                  (code.text.length == 6) ? pressVerifyOTP(
                      widget.email,
                      code.text,
                      (){
                        print('ok');
                        showGeneratePassword(context);
                        },
                      (String e){showError(context, e);}
                  ) : null;
                },
              ),
              SizedBox(height: 40.w),
            ],
          ),
        ),
      ),
    );
  }
}