import 'package:flutter/material.dart';
import 'package:prof2024/presentation/pages/tabs/favorite_tab.dart';
import 'package:prof2024/presentation/pages/tabs/home_tab.dart';
import 'package:prof2024/presentation/pages/tabs/notification_tab.dart';
import 'package:prof2024/presentation/pages/tabs/profile_tab.dart';
import 'package:prof2024/presentation/theme/colors.dart';
import 'package:prof2024/presentation/widgets/custom_nav_bar.dart';

class Home extends StatefulWidget {
  final int currentIndex;
  const Home({super.key, this.currentIndex = 0});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState(){
    super.initState();
    setState(() {
      currentIndex = widget.currentIndex;
    });
  }
  var pages = [HomeTab(), FavoriteTab(), NotificationTab(), ProfileTab()];
  var currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.block,
      body: Stack(
        children: [
          pages[currentIndex],
          Align(
            alignment: Alignment.bottomCenter,
            child: CustomBar(
                onSelectTap: (newIndex){
                  setState(() {
                    currentIndex = newIndex;
                  });
                },
                onCardTap: (){}
            ),
          )
        ],
      ),
    );
  }
}