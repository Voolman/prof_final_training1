import 'package:prof2024/data/models/ModelProduct.dart';
import 'package:prof2024/data/repository/supabase.dart';
import 'package:prof2024/data/storage/products.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

void insertCart(ModelProduct product){
  var index = allProducts.indexOf(product);
  product.isCart = !product.isCart;
  allProducts[index] = product;
}

Future<void> sendCart(Function onResponse, Function(String) onError) async {
  try{
    await insertOrder();
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.message);
  }on Exception catch(e){
    onError(e.toString());
  }
}