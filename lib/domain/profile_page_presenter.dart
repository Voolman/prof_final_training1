import 'dart:typed_data';

import 'package:image_picker/image_picker.dart';
import 'package:prof2024/data/repository/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> pressAvatarCamera(Function onPick) async {
  XFile? avatarFile = await ImagePicker().pickImage(source: ImageSource.camera);
  if(avatarFile != null){
    Uint8List avatar = await avatarFile.readAsBytes();
    onPick(avatar);
  }
}

Future<void> pressAvatarGallery(Function onPick) async {
  XFile? avatarFile = await ImagePicker().pickImage(source: ImageSource.gallery);
  if(avatarFile != null){
    Uint8List avatar = await avatarFile.readAsBytes();
    onPick(avatar);
  }
}

Future<void> updateUserMetaData(Map<String, dynamic> json, Function onResponse, Function(String) onError) async {
  try{
    await updateMetaData(json);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.message);
  }on Exception catch(e){
    onError(e.toString());
  }
}