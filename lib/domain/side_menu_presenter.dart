import 'package:prof2024/data/repository/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> pressSignOut(Function onResponse, Function(String) onError) async {
  try{
    await signOut();
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.message);
  }on Exception catch(e){
    onError(e.toString());
  }
}

Map<String, dynamic>? getUserMetaData(){
  return getMetaData();
}