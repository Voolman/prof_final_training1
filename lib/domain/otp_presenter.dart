import 'package:prof2024/data/repository/supabase.dart';
import 'package:prof2024/domain/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> pressVerifyOTP(String email,String code, Function onResponse, Function(String) onError) async {
  try{
    await verifyOTP(email, code);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.message);
  }on Exception catch(e){
    onError(e.toString());
  }
}

String createPassword(String password){
  return password.split('').reversed.join();
}

Future<void> pressUpdatePassword(String password, Function onResponse, Function(String) onError) async {
  try{
    await changePassword(password);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.message);
  }on Exception catch(e){
    onError(e.toString());
  }
}