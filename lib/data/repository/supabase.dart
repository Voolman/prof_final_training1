import 'package:supabase_flutter/supabase_flutter.dart';

final supabase = Supabase.instance.client;

Future<AuthResponse> signIn(String email, String password) async {
  return await supabase.auth.signInWithPassword(email: email, password: password);
}

Future<AuthResponse> signUp(String email, String password, String name) async {
  return await supabase.auth.signUp(email: email, password: password, data: {'name': name});
}

Future<void> sendOTP(String email) async {
  return await supabase.auth.resetPasswordForEmail(email);
}

Future<AuthResponse> verifyOTP(String email, String code) async {
  return await supabase.auth.verifyOTP(email: email, token: code, type: OtpType.email);
}

Future<UserResponse> changePassword(String password) async {
  return await supabase.auth.updateUser(UserAttributes(password: password));
}

Future<void> signOut() async {
  return await supabase.auth.signOut();
}

Future<PostgrestList> getCategory() async {
  return await supabase.from('categories').select();
}

Future<PostgrestList> getProducts() async {
  return await supabase.from('products').select();
}

Future<List<String>> getImagesForProduct(String id) async {
  List<FileObject> response =  await supabase.storage.from('images').list();
  List<String> currentImages = [];
  for(int i = 0; i<response.length;i++){
    if(response[i].name.contains(id)){
      currentImages.add(
        await supabase.storage.from('images').getPublicUrl(response[i].name)
      );
    }
  }
  return currentImages;
}

bool currentUser(){
  return supabase.auth.currentUser?.id == null;
}

Future<PostgrestList> getFavorite() async {
  return await supabase.from('favorite').select().eq('id_user', supabase.auth.currentUser!.id);
}

Future<PostgrestList> getCart() async {
  return await supabase.from('cart').select().eq('id_user', supabase.auth.currentUser!.id);
}

Future<PostgrestList> insertCart(String idProduct) async {
  return await supabase.from('cart').insert({'id_product': idProduct, 'id_user':supabase.auth.currentUser!.id});
}

Future<PostgrestList> deleteCart(String idProduct) async {
  return await supabase.from('cart').delete().eq('id_user', supabase.auth.currentUser!.id).eq('id_product', idProduct);
}

Future<void> insertOrder() async {
  return await supabase.from('order').insert({'email': supabase.auth.currentUser!.email, 'phone': '999', 'address': null, 'user_id': supabase.auth.currentUser!.id, 'delivery_cost': 100});
}

Map<String, dynamic>? getMetaData(){
  return supabase.auth.currentUser?.userMetadata;
}

Future<UserResponse> updateMetaData(Map<String, dynamic> json) async {
  return await supabase.auth.updateUser(UserAttributes(
    data: json
  ));
}



